# Setup SSH Keys
sed -ie 's/#Port 22/Port 22/g' /etc/ssh/sshd_config
sed -ir 's/#HostKey \/etc\/ssh\/ssh_host_key/HostKey \/etc\/ssh\/ssh_host_key/g' /etc/ssh/sshd_config
sed -ir 's/#HostKey \/etc\/ssh\/ssh_host_rsa_key/HostKey \/etc\/ssh\/ssh_host_rsa_key/g' /etc/ssh/sshd_config
sed -ir 's/#HostKey \/etc\/ssh\/ssh_host_dsa_key/HostKey \/etc\/ssh\/ssh_host_dsa_key/g' /etc/ssh/sshd_config
sed -ir 's/#HostKey \/etc\/ssh\/ssh_host_ecdsa_key/HostKey \/etc\/ssh\/ssh_host_ecdsa_key/g' /etc/ssh/sshd_config
sed -ir 's/#HostKey \/etc\/ssh\/ssh_host_ed25519_key/HostKey \/etc\/ssh\/ssh_host_ed25519_key/g' /etc/ssh/sshd_config

/usr/bin/ssh-keygen -A
ssh-keygen -t rsa -b 4096 -f  /etc/ssh/ssh_host_key

# Sets password for user "root" for SSH / SFTP
echo "root:$SSH_ROOT_PASSWORD" | chpasswd

# Creates and sets password for user "$SSH_ADMIN_USERNAME" for SSH / SFTP
adduser -D ${SSH_ADMIN_USERNAME}
echo "${SSH_ADMIN_USERNAME}:${SSH_ADMIN_PASSWORD}" | chpasswd

# Grant permission for user "$SSH_ADMIN_USERNAME" to access to $SSH_STARTUP_DIRECTORY
echo "Fixing permissions for user ${SSH_ADMIN_USERNAME}"
chown -R $SSH_ADMIN_USERNAME:$SSH_ADMIN_USERNAME $SSH_STARTUP_DIRECTORY

# Sets default SSH directory for users "root" and "$SSH_ADMIN_USERNAME"
mkdir $SSH_STARTUP_DIRECTORY
echo "cd $SSH_STARTUP_DIRECTORY" >> ~/.profile

# Create symbolic link for admin convenient when logging in using SFTP
ln -s $GOOFYS_MOUNT_DIR /home/$SSH_ADMIN_USERNAME/s3

# Copying .profile and .bashrc from user "root" to user "$SSH_ADMIN_USERNAME"
cp ~/.profile /home/$SSH_ADMIN_USERNAME/.profile
cp ~/.bashrc /home/$SSH_ADMIN_USERNAME/.bashrc
cp ~/.vimrc /home/$SSH_ADMIN_USERNAME/.vimrc

# Fixes SSH terminal display error (esp. when using VI) for users "root" and "$SSH_ADMIN_USERNAME"
echo "shopt -s checkwinsize" >> ~/.bashrc 
echo "set nocompatible" >> ~/.vimrc
echo "set backspace=2" >> ~/.vimrc

# Run SSH Daemon
/usr/sbin/sshd

# Setup goofys (for S3 mount)
syslog-ng
goofys -f -o allow_other --region $AWS_REGION --uid "$(id -u $SSH_ADMIN_USERNAME)" --gid "$(id -g $SSH_ADMIN_USERNAME)" --stat-cache-ttl $GOOFYS_STAT_CACHE_TTL --type-cache-ttl $GOOFYS_TYPE_CACHE_TTL --dir-mode $GOOFYS_DIR_MODE --file-mode $GOOFYS_FILE_MODE $AWS_BUCKET_AND_PREFIX $GOOFYS_MOUNT_DIR
